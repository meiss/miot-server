package org.springblade.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

/**
 * 初始化MQTT客户端
 * @author 李家民
 */
@Component
public class MqttInitialized {

	/** MQTT客户端 */
	private static MqttClient client = null;
	/** 连接选项 */
	private static MqttConnectOptions connOpts = null;
	/** 连接状态 */
	private static Boolean connectStatus = false;


	/**
	 * 获取MQTT客户端连接状态
	 * @return
	 */
	public static Boolean getConnectStatus() {
		return connectStatus;
	}

	static {
		try {
			// MQTT 连接选项
			connOpts = new MqttConnectOptions();
			// 设置认证信息
			connOpts.setUserName(MqttConfig.USERNAME);
			connOpts.setPassword(MqttConfig.PASSWD.toCharArray());
			//  持久化
			MemoryPersistence persistence = new MemoryPersistence();
			// MQ客户端建立
			client = new MqttClient(MqttConfig.IP_ADDRESS, MqttClient.generateClientId(), persistence);
			// 设置回调
			client.setCallback(new MqttHandle());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * MQTT客户端启动
	 */
	@PostConstruct
	public static void connect() {
		try {
			// 建立连接
			client.connect(connOpts);
			connectStatus = client.isConnected();
			System.out.println("MQTT服务器连接成功~~~");
		} catch (Exception e) {
			connectStatus = client.isConnected();
			System.out.println("MQTT服务器连接失败!!");
			e.printStackTrace();
			reconnection();
		}
	}

	/**
	 * 消息订阅
	 * @param topic 主题
	 * @param qos   QOS
	 */
	public void subscribe(String topic, Integer qos) throws MqttException {
		client.subscribe(topic, qos);
	}

	/**
	 * 消息发布
	 * @param topic   主题
	 * @param message 消息体
	 * @param qos     QOS
	 */
	public void publish(String topic, String message, Integer qos) throws MqttException {
		MqttMessage mqttMessage = new MqttMessage(message.getBytes());
		mqttMessage.setQos(qos);
		client.publish(topic, mqttMessage);
	}

	/**
	 * 断线重连
	 */
	public static void reconnection() {
		// 尝试进行重新连接
		while (true) {
			if (MqttInitialized.getConnectStatus()) {
				// 查询连接状态 连接成功则停止重连
				break;
			}
			try {
				System.out.println("开始进行MQTT服务器连接.......");
				// 进行连接
				connect();
				Thread.sleep(10000);
			} catch (Exception e) {
				System.out.println("重新连接出现异常");
				e.printStackTrace();
				break;
			}
		}
	}

	/**
	 * 测试方法 进行消息订阅
	 */
	public void test() {
		try {
			subscribe(MqttConfig.TOPIC, MqttConfig.QOS);
		} catch (MqttException e) {
			throw new RuntimeException(e);
		}
	}
}
