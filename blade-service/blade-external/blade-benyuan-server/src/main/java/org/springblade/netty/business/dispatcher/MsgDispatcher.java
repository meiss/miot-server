package org.springblade.netty.business.dispatcher;


import org.springblade.common.tool.ThreadPoolUtils;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.StringUtil;
import org.springblade.netty.business.MsgCode;
import org.springblade.netty.business.handle.system.AuditLogHandler;
import org.springblade.netty.business.handle.tag.INotAuthProcessor;
import org.springblade.netty.protocol.GameSession;
import org.springblade.netty.protocol.request.ServerRequest;
import org.springblade.netty.protocol.response.SendResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


/**
 * 消息分发器 根据消息号 找到相应的消息处理器
 * @author xxx
 */
public class MsgDispatcher {

	private Map<Integer, MsgProcessor> processorsMap = new HashMap<Integer, MsgProcessor>();

	public MsgDispatcher() {
		for (MsgProcessorRegister register : MsgProcessorRegister.values()) {
			processorsMap.put(register.getMsgCode(), register.getProcessor());
		}
		System.out.println("初始化 消息处理器成功 --- ");
	}

	/**
	 * 通过协议号得到MsgProcessor
	 * @param msgCode
	 * @return
	 */
	public MsgProcessor getMsgProcessor(int msgCode) {
		return processorsMap.get(msgCode);
	}

	/**
	 * 派发消息协议
	 * @param gameSession
	 * @param serverRequest
	 */
	public R dispatchMsg(GameSession gameSession, ServerRequest serverRequest) {
		MsgProcessor processor = getMsgProcessor(serverRequest.getCode());
		try {
			// 是否登录判断
			if (StringUtil.isNotBlank(gameSession.getUser()) || processor instanceof INotAuthProcessor) {
				// ... 权限控制通过 User 后续补充
				if (serverRequest.getCode() != MsgCode.CODE_30.getReq()) {
					AuditLogHandler.pushAuditLog(gameSession.getChannelHandlerContext(), serverRequest);
				}
				CompletableFuture.runAsync(
					() -> processor.handle(gameSession, serverRequest)
					, ThreadPoolUtils.getThreadPool());
				return R.status(true);
			} else {
				gameSession.sendMessage(new SendResponse(403, R.data("Please login the system...."), MsgCode.CODE_1.getResp(), "Please login the system...."));
				return R.fail("Please login the system....");
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			return R.fail("存在未实现的接口:接口码 = " + serverRequest.getCode());
		}
	}
}
