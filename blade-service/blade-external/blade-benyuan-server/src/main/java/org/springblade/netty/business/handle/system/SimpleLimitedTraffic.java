package org.springblade.netty.business.handle.system;

import org.springblade.common.tool.ThreadPoolUtils;
import org.springblade.netty.NettyServerInboundHandler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 简易限流器
 * @author 李家民
 */
@Component
public class SimpleLimitedTraffic {

	/** 轮询时长 毫秒 */
	private static long durationPolling = 10000;

	/** 单位时间内请求最大次数 */
	private static int maximum = 20;

	/** 流量桶 */
	private static ConcurrentHashMap<String, AtomicInteger> bucketTraffic = new ConcurrentHashMap<>();

	/**
	 * 添加流量
	 * @param address
	 */
	public static void addTrafficOnBucket(String address) {
		if (bucketTraffic.containsKey(address)) {
			AtomicInteger atomicInteger = bucketTraffic.get(address);
			atomicInteger.addAndGet(1);
			bucketTraffic.put(address,atomicInteger);
		}else {
			bucketTraffic.put(address, new AtomicInteger(0));
		}
	}

	/**
	 * 移除地址
	 * @param address
	 */
	public static void removeAddressBucket(String address) {
		bucketTraffic.remove(address);
	}

	/**
	 * 桶监听
	 */
	@PostConstruct
	private void bucketListening() {
		CompletableFuture.runAsync(() -> {
			try {
				while (true) {
					Thread.sleep(durationPolling);
					bucketTraffic.forEach((k, v) -> {
						if (v.get() > maximum) {
							NettyServerInboundHandler.closeGameSession(k);
							removeAddressBucket(k);
						} else {
							bucketTraffic.put(k, new AtomicInteger(0));
						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}, ThreadPoolUtils.getThreadPool());
	}
}
