package org.springblade.netty.protocol;

import io.netty.channel.ChannelHandlerContext;
import lombok.Data;
import lombok.Getter;
import org.springblade.common.constant.NumberConstant;
import org.springblade.netty.protocol.response.ResponseMsg;
import org.springblade.system.user.entity.User;

/**
 * 消息通道的包装
 * @author 李家民
 */
@Data
@Getter
public class GameSession {

	/** 登录的用户信息 */
	private String user;

	/** 通道 */
	private ChannelHandlerContext channelHandlerContext;

	/**
	 * 构造方法
	 * @param channelHandlerContext 通道
	 */
	public GameSession(ChannelHandlerContext channelHandlerContext) {
		this.channelHandlerContext = channelHandlerContext;
	}

	/**
	 * 消息发送
	 * @param msg 消息体
	 */
	public void sendMessage(ResponseMsg msg) {
		if (channelHandlerContext.channel().isActive()) {
			channelHandlerContext.writeAndFlush(msg);
		}
	}

}
