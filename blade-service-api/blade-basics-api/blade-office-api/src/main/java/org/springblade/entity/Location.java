/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import org.springblade.core.mp.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类
 * @author Blade
 * @since 2022-08-26
 */
@Data
@TableName("blade_location")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Location对象", description = "Location对象")
public class Location extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 名称
	 */
	@ApiModelProperty(value = "名称")
	private String name;
	/**
	 * 位置类型，1办公室，2开放办公区，3待定
	 */
	@ApiModelProperty(value = "位置类型，1办公室，2开放办公区，3待定")
	private Integer type;
	/**
	 * 房间号
	 */
	@ApiModelProperty(value = "房间号")
	private Integer room;
	/**
	 * 楼层号
	 */
	@ApiModelProperty(value = "楼层号")
	private Integer floor;
	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String remark;


}
