/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.core.log.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.log.entity.LogAudit;
import org.springblade.core.log.vo.LogAuditVO;
import org.springblade.core.log.service.ILogAuditService;
import org.springblade.core.boot.ctrl.BladeController;

/**
 *  控制器
 *
 * @author Blade
 * @since 2022-08-28
 */
@RestController
@AllArgsConstructor
@RequestMapping("/logaudit")
@Api(value = "", tags = "接口")
public class LogAuditController extends BladeController {

	private ILogAuditService logAuditService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入logAudit")
	public R<LogAudit> detail(LogAudit logAudit) {
		LogAudit detail = logAuditService.getOne(Condition.getQueryWrapper(logAudit));
		return R.data(detail);
	}

	/**
	 * 分页 
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入logAudit")
	public R<IPage<LogAudit>> list(LogAudit logAudit, Query query) {
		IPage<LogAudit> pages = logAuditService.page(Condition.getPage(query), Condition.getQueryWrapper(logAudit));
		return R.data(pages);
	}

	/**
	 * 自定义分页 
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入logAudit")
	public R<IPage<LogAuditVO>> page(LogAuditVO logAudit, Query query) {
		IPage<LogAuditVO> pages = logAuditService.selectLogAuditPage(Condition.getPage(query), logAudit);
		return R.data(pages);
	}

	/**
	 * 新增 
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入logAudit")
	public R save(@Valid @RequestBody LogAudit logAudit) {
		return R.status(logAuditService.save(logAudit));
	}

	/**
	 * 修改 
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入logAudit")
	public R update(@Valid @RequestBody LogAudit logAudit) {
		return R.status(logAuditService.updateById(logAudit));
	}

	/**
	 * 新增或修改 
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入logAudit")
	public R submit(@Valid @RequestBody LogAudit logAudit) {
		return R.status(logAuditService.saveOrUpdate(logAudit));
	}

	
	/**
	 * 删除 
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@ApiOperation(value = "逻辑删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(logAuditService.deleteLogic(Func.toLongList(ids)));
	}

	
}
