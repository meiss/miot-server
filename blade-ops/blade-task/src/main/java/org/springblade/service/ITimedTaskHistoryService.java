package org.springblade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springblade.entity.TimedTaskHistory;

public interface ITimedTaskHistoryService  extends IService<TimedTaskHistory> {
}
