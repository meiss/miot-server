package org.springblade.business;

import org.springblade.aop.TaskThreadHandle;
import org.springblade.core.tool.api.R;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 定时任务示例Demo
 * @author 李家民
 */
@Component
@EnableScheduling
public class SimpleBusiness {

	/** 上一次执行完毕时间后执行下一轮 --- 每次5秒 缺点：任务之间会发生线程阻塞 所以那个@TaskThreadHandle注解要套上 */
	@Scheduled(cron = "0/5 * * * * *")
	@TaskThreadHandle
	public R demoRunSync() throws InterruptedException {
		long startTime = System.currentTimeMillis();
		Thread.sleep(10000);
		System.out.println("=====>>>>>使用cron demoRunSync() {}");
		long lastTime = System.currentTimeMillis();
		System.out.println("demoRunSync()执行时长 = " + (lastTime - startTime) / 1000 + " 秒");
		return R.status(true);
	}

	/**
	 * 会议状态/会议室状态定时刷新
	 * 每小时的x分执行一次
	 */
	@Scheduled(cron = "0 5,20,50 * * * ?")
	@TaskThreadHandle
	public R meetingStatusUpdate() {
		System.out.println("会议状态/会议室状态定时刷新 = " + LocalDateTime.now());
		return R.status(true);
	}

	/**
	 * 访客状态定时刷新
	 * 每天凌晨1点刷新
	 */
	@Scheduled(cron = "0 0 1 * * ?")
	@TaskThreadHandle
	public R visitStatusUpdate() {
		System.out.println("访客状态定时刷新 = " + LocalDateTime.now());
		return R.status(true);
	}

}
