/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.resource.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springblade.core.mp.base.BaseEntity;

/**
 * 附件表实体类
 * @author Blade
 * @since 2022-10-03
 */
@Data
@TableName("blade_attach")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Attach对象", description = "附件表")
@NoArgsConstructor
public class Attach extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	private Long id;
	/**
	 * 附件地址
	 */
	@ApiModelProperty(value = "附件地址")
	private String link;
	/**
	 * 附件域名
	 */
	@ApiModelProperty(value = "附件域名")
	private String domainUrl;
	/**
	 * 附件名称
	 */
	@ApiModelProperty(value = "附件名称")
	private String name;
	/**
	 * 附件原名
	 */
	@ApiModelProperty(value = "附件原名")
	private String originalName;
	/**
	 * 附件拓展名
	 */
	@ApiModelProperty(value = "附件拓展名")
	private String extension;
	/**
	 * 附件大小-字节
	 */
	@ApiModelProperty(value = "附件大小")
	private Long attachSize;

	public Attach(String link, String domainUrl, String name, String originalName, String extension, Long attachSize) {
		this.link = link;
		this.domainUrl = domainUrl;
		this.name = name;
		this.originalName = originalName;
		this.extension = extension;
		this.attachSize = attachSize;
	}
}
