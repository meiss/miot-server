package org.springblade.common.view;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 折线视图通用
 * @author lijiamin
 */
@Data
@NoArgsConstructor
public class ChartView<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 标题值 */
	private Map<String, Map<String,String>> titles;

	/** 时间线 */
	private List<String> times;

	/** 数据集 */
	private Map<String, List<T>> data;

}
