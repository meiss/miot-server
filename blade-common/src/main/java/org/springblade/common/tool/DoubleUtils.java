package org.springblade.common.tool;

import cn.hutool.core.util.ObjectUtil;

import java.math.BigDecimal;

public class DoubleUtils {

	/**
	 * double除法
	 * @param a
	 * @param b
	 * @param accurate
	 * @return
	 */
	public static double division(double a, double b, int accurate) {
		if (accurate < 0) {
			throw new RuntimeException("精确度必须是正整数或零");
		}
		if (ObjectUtil.equal(0.0 , b)){
			return 0.0;
		}
		BigDecimal b1 = new BigDecimal(a);
		BigDecimal b2 = new BigDecimal(b);
		return b1.divide(b2, accurate, BigDecimal.ROUND_HALF_UP).doubleValue();
	}


	/**
	 * 提供精确的加法运算。
	 * @param value1 被加数
	 * @param value2 加数
	 * @return 两个参数的和
	 */
	public static Double add(Double value1, Double value2) {
		BigDecimal b1 = new BigDecimal(Double.toString(value1));
		BigDecimal b2 = new BigDecimal(Double.toString(value2));
		return b1.add(b2).doubleValue();

	}

	/**
	 * 提供精确的减法运算。
	 * @param value1 被减数
	 * @param value2 减数
	 * @return 两个参数的差
	 */
	public static double sub(Double value1, Double value2) {
		BigDecimal b1 = new BigDecimal(Double.toString(value1));
		BigDecimal b2 = new BigDecimal(Double.toString(value2));
		return b1.subtract(b2).doubleValue();

	}

	/**
	 * 提供精确的乘法运算。
	 * @param value1 被乘数
	 * @param value2 乘数
	 * @return 两个参数的积
	 */
	public static Double mul(Double value1, Double value2) {
		BigDecimal b1 = new BigDecimal(Double.toString(value1));
		BigDecimal b2 = new BigDecimal(Double.toString(value2));
		return b1.multiply(b2).doubleValue();
	}
}

